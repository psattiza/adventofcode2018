import sys

n = 0
s = set()
s.add(0)
deltas=[]

for line in sys.stdin:
    i = int(line.strip())
    deltas.append(i)
    n += i
    if n in s:
        print("Match found!")
        print(n)
        break
    s.add(n)

while(True):
    for i in deltas:
        n += i
        if n in s:
            print("Match found!")
            print(n)
            exit(1)
        s.add(n)

print("No match found. Last n is:")
print(n)
