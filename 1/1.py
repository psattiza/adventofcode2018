import sys

n = 0

for line in sys.stdin:
    n += int(line.strip())

print(n)
