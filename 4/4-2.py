import sys
import re
from datetime import datetime
from collections import defaultdict
import numpy as np


elfSleep = {}
elfSleep = defaultdict(lambda:[0]*60,elfSleep)

# print(elfSleep[1])

# a = [line.strip() for line in sys.stdin]
#for i in range(len(a)):

events = []

for line in sys.stdin:
    line = line.strip()

    x = re.match("\[(.*)\] (.*)",line)
    dateStr = x.group(1)
    eventStr = x.group(2)
    # print("'{}' '{}'".format(dateStr, eventStr))

    date = datetime.strptime(dateStr, "%Y-%m-%d %H:%M")
    # print("\t", date)

    events.append((date, eventStr))

events.sort(key=lambda tup: tup[0])

currentElfNum=-1
currentSleepStart=-1
currentSleepEnd=-1
for i in events:
    d = i[0]
    words = i[1].split()
    # print("{}=>{}, '{}'".format(i,d,words))

    if words[0] == "Guard":
        currentElfNum= int(words[1][1:])
        currentSleepStart=-1
        currentSleepEnd=-1
        print("\tGuard num =",currentElfNum)
    elif words[0] == "falls":
        currentSleepStart = d.minute
        print("\tfall @", currentSleepStart)
    elif words[0] == "wakes":
        currentSleepEnd= d.minute
        print("\twake @", currentSleepEnd)
        print("Elf#{}, slept from {}-{}".format( currentElfNum, currentSleepStart, currentSleepEnd-1))

        for minOfSleep in range(currentSleepStart, currentSleepEnd):
            elfSleep[currentElfNum][minOfSleep]+=1

    else:
        print("FOUND NATHIN************************************")
        print(words)
        exit(0)


maxSleep =-1
sleepyElf=-1
sleepiestTime=-1

allTimes=[]
for elfNum, sleepyTimes in elfSleep.items():
    print("{:>4}".format(elfNum), ">>", end="")
    for i in range(60):
        print(i,":",sleepyTimes[i]," ",sep="", end="")
    print()

    sleepiestTimeForElf = sleepyTimes.index(max(sleepyTimes))
    if sleepiestTimeForElf > maxSleep:
        maxSleep = sleepiestTimeForElf
        sleepyElf=elfNum
        sleepiestTime = sleepiestTimeForElf
    allTimes.append(sleepyTimes)
    
allTimes=np.array(allTimes)
print("++++",max(allTimes.flatten()))



print("Elf:{} x Slept:{} @ min#{} = {} ".format(sleepyElf, maxSleep,sleepiestTime,sleepyElf*sleepiestTime ))

print("".format())
