import sys
from collections import defaultdict

a = [line.strip() for line in sys.stdin]

for i in range(len(a)):
    x=a[i]
    ##print(x)
    for j in range(i+1,len(a)):
        #print("a:{} i:{}".format(a,j))
        y=a[j]
        #print(y)
        minLen = min(len(y), len(x))


        miss=0
        missIndex=-1
        for k in range(minLen):
            xx=x[k]
            yy=y[k]
            
            #print("{} vs {} @{} {}=?={}".format( x, y, a, xx, yy))
            if xx!=yy:
                miss+=1
                missIndex=k

        if miss==1:
            print("Miss by 1!")
            print(x)
            print(y)
            print(x[0:missIndex] + x[missIndex+1:])
            exit(0)
            
    

print(a)
    
