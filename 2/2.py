import sys
from collections import defaultdict

n = 0

def convertToFreq(s):
    d=defaultdict(int)
    for c in s:
        d[c]+=1
    return d


def containsDuble(s):
    return True

def containsTrip(s):
    return False

dubs =0
trips =0

for line in sys.stdin:
    s = line.strip()
    d = convertToFreq(s)

    dub=False
    trip=False
    
    for k,v in d.items():
        if v==3:
            print("Trip {} {}=>{}".format(s, k,v))
            trip=True
        if v==2:
            print("Dub {} {}=>{}".format(s, k,v))
            dub=True

    if trip:
        trips+=1
    if dub:
        dubs+=1

print(dubs)
print(trips)
print(dubs*trips)
