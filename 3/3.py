import sys
import re
from collections import defaultdict
import numpy as np

fab = np.zeros((2000, 2000))

a = [[int(w)  for w in line.strip().split()] for line in sys.stdin]

for i in a:
    iid = i[0]
    y=i[1]
    x=i[2]
    dy=i[3]
    dx=i[4]

    for cy in range(y,y+dy):
        for cx in range(x,x+dx):
            fab[cy][cx]+=1
for i in a:
    iid = i[0]
    y=i[1]
    x=i[2]
    dy=i[3]
    dx=i[4]
    bad=False
    for cy in range(y,y+dy):
        for cx in range(x,x+dx):
            if fab[cy][cx]!=1:
                bad=True
                break
        if bad:
            break
    if not bad:
        print(iid)



# for line in fab:
#     for n in line:
#         print(int(n), end="")
#     print()
tot =0

for number in fab.ravel():
    if number>=2:
            tot+=1

print(tot)

exit(0)
for i in range(len(a)):
    x=a[i]
    ##print(x)
    for j in range(i+1,len(a)):
        #print("a:{} i:{}".format(a,j))
        y=a[j]
        #print(y)
        minLen = min(len(y), len(x))


        miss=0
        missIndex=-1
        for k in range(minLen):
            xx=x[k]
            yy=y[k]
            
            #print("{} vs {} @{} {}=?={}".format( x, y, a, xx, yy))
            if xx!=yy:
                miss+=1
                missIndex=k

        if miss==1:
            print("Miss by 1!")
            print(x)
            print(y)
            print(x[0:missIndex] + x[missIndex+1:])
            exit(0)
            
    

print(a)
    
